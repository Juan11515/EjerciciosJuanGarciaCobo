import java.util.Random;
import java.util.Scanner;

public class Ahorcado {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int INTENTOS_TOTALES = 5;
		int intentos = 0;
		int aciertos = 0;

		Scanner teclado = new Scanner(System.in);
		teclado.useDelimiter("\n");
		char resp;

		Random rnd = new Random();

		String arrayPalabras[] = new String[7];
		arrayPalabras[0] = "hola";
		arrayPalabras[1] = "adios";
		arrayPalabras[2] = "cuadrado";
		arrayPalabras[3]= "cancun";
		arrayPalabras[4]= "ut";
		arrayPalabras[5]= "tic";
		arrayPalabras[6]="OrgulloUT";
		do {

			int alea = rnd.nextInt(7);
			char[] separador = separador(arrayPalabras[alea]);
			char[] copia = separador(arrayPalabras[alea]);
			char[] tusRespuestas = new char[separador.length];

			for (int i = 0; i < tusRespuestas.length; i++) {
				tusRespuestas[i] = '_';
			}

			System.out.println("Adivina la palabra sino mueres UwU!");

			while (intentos < INTENTOS_TOTALES && aciertos != tusRespuestas.length) {
				imprimeOculta(tusRespuestas);

				System.out.println("\nIngresa una letra: ");
				resp = teclado.next().toLowerCase().charAt(0);

				for (int i = 0; i < separador.length; i++) {
					if (separador[i] == resp) {
						tusRespuestas[i] = separador[i];
						separador[i] = ' ';
						aciertos++;
					}
				}
				intentos++;
			}

			if (aciertos == tusRespuestas.length) {
				System.out.println("\n--Fin del juego--");
				System.out.println("     ///////");
				System.out.println("    *  u-u  *");
				System.out.println("    *   U   * ");
				System.out.println("     *******");
				System.out.println("      / |----b");
				System.out.println("     /  | ");
				System.out.println("        |");
				System.out.println("        |");
				System.out.println("      / * /");
				System.out.println("     /   /");
				System.out.println("    /   /");
				System.out.print("\nFalicidades!! has acertado la palabra: ");
				imprimeOculta(tusRespuestas);
			}

			else {
				System.out.println("\n--Fin del juego--");
				System.out.println("     ///////");
				System.out.println("    *  x-x  *");
				System.out.println("    *   n   * ");
				System.out.println("     *******");
				System.out.println("      / |");
				System.out.println("     / /| ");
				System.out.println("      / |");
				System.out.println("        |");
				System.out.println("      / * /");
				System.out.println("     /   /");
				System.out.println("    /   /");
				System.out.print("\nPerdiste estas muerto! la palabra era: ");
				for (int i = 0; i < copia.length; i++) {
					
					System.out.print(copia[i] + " ");
							
				}
			}
			intentos = 0;
			aciertos = 0;
			resp = pregunta("\n\nQuieres jugar de nuevo?", teclado);
		} while (resp != 'n');

	}

	private static char[] separador(String palAzar) {
		char[] letras;
		letras = new char[palAzar.length()];
		for (int i = 0; i < palAzar.length(); i++) {
			letras[i] = palAzar.charAt(i);
		}
		return letras;
	}

	private static void imprimeOculta(char[] tusRespuestas) {

		for (int i = 0; i < tusRespuestas.length; i++) {
			System.out.print(tusRespuestas[i] + " ");
		}
	}

	public static char pregunta(String men, Scanner teclado) {
		char resp;
		System.out.println(men + " (si/no)");
		resp = teclado.next().toLowerCase().charAt(0);
		while (resp != 's' && resp != 'n') {
			System.out.println("Error! Solo se admite Si o No");
			resp = teclado.next().toLowerCase().charAt(0);
		}
		return resp;
	}

}