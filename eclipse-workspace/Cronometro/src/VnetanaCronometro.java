import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class VnetanaCronometro extends JFrame implements ActionListener {

	private JLabel label;
	private JButton btnIniciar;
	private JButton btnParar;

	static int hora = 0, minuto = 0, segundos = 0;

	static boolean iniciarContador = true;
	boolean Iniciando = false;

	public VnetanaCronometro() {
		initialice();
		setSize(228, 120);
		getContentPane().setLayout(null);
		setLocationRelativeTo(null);
	}

	private void initialice() {
		// TODO Auto-generated method stub
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		label = new JLabel("00:00:00");
		label.setFont(new Font("traditional arabic", Font.PLAIN, 30));
		label.setBounds(48, 12, 137, 45);
		getContentPane().add(label);

		btnIniciar = new JButton("Iniciar");
		btnIniciar.setBounds(119, 54, 89, 23);
		btnIniciar.addActionListener(this);
		getContentPane().add(btnIniciar);

		btnParar = new JButton("Para");
		btnParar.setBounds(10, 54, 89, 23);
		btnParar.addActionListener(this);
		getContentPane().add(btnParar);

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnIniciar) {
			if (Iniciando == false) {
				iniciarContador = true;
				Iniciando = true;
				iniciarContadorCronometro();
			}
		}
		if (e.getSource() == btnParar) {
			Iniciando = false;
			iniciarContador = false;
		}
	}

	private void iniciarContadorCronometro() {
		// TODO Auto-generated method stub

		if (iniciarContador == true) {
			System.out.println("Iniciando Cronometro");
			Cronometro miCronometro = new Cronometro(label);
			miCronometro.start();
		}
	}
}
