import javax.swing.JLabel;

public class Cronometro extends Thread {

	JLabel Eti;

	public Cronometro(JLabel Cronometro) {

		this.Eti = Cronometro;
	}

	public void run() {
		try {
			int x =0;
			
			while (VnetanaCronometro.iniciarContador) {
				Thread.sleep(10);
				System.out.println(x);
				ejecutariniciarCronometro(x);
				x++;
			
			}
		} catch (Exception e) {
			System.out.println("Exepcion de Iniciar: "+e.getMessage());
		}
	}

	private void ejecutariniciarCronometro(int x) {
		// TODO Auto-generated method stub
		System.out.println(x+" - "+Thread.currentThread().getName());
		VnetanaCronometro.segundos++;
		
		if(VnetanaCronometro.segundos>59) {
			VnetanaCronometro.segundos=0;
			VnetanaCronometro.minuto++;
		} 
		if(VnetanaCronometro.minuto>59) {
			VnetanaCronometro.minuto=0;
			VnetanaCronometro.hora++;
		}
		String textSeg=" ",textMin=" ",textHora=" ";
		textSeg+=VnetanaCronometro.segundos;
		textMin+=VnetanaCronometro.minuto;
		textHora+=VnetanaCronometro.hora;
		
		String reloj=textHora+":"+textMin+":"+textSeg;
		Eti.setText(reloj);
	}

}
