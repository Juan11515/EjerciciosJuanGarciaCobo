public class Reloj {
	public static void main(String[] args) throws InterruptedException {
		int horas, minutos, segundos;
		horas = 0;
		minutos = 0;
		segundos = 0;
		int dias = 0;
		while (true) {
			Thread.sleep(1000);
			segundos = segundos + 1;
			if (segundos > 59) {
				segundos = 0;
				minutos = minutos + 1;
			}
			if (minutos > 59) {
				minutos = 0;
				horas = horas + 1;
			}

			if (dias > 59) {
				dias = 0;
				dias = dias + 1;

			}
			System.out.println("=================");
			System.out.println("[Reloj: " +dias+":"+ horas + ":" + minutos + ":" + segundos + " ]");
			System.out.println("=================");
		}
	}
}
